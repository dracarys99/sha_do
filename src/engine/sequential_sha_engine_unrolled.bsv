package sequential_sha_engine_unrolled;

import FIFOF        :: *;
import GetPut       :: *;
import utils        :: *;

typedef enum{IDLE,HASH,OUTPUT} State deriving(Bits,Eq);

interface Ifc_sha_engine;
  method Action reset;
  method Bool ready;
  method Action input_engine(Bit#(256) pre_hash,Bit#(512) input_val);
  interface Get#(Bit#(256)) output_engine;
endinterface

function Bit#(32) k_o(Bit#(6) o);
  case (o)
    0:return('h428a2f98);
    1:return('hb5c0fbcf);
    2:return('h3956c25b);
    3:return('h923f82a4);
    4:return('hd807aa98);
    5:return('h243185be);
    6:return('h72be5d74);
    7:return('h9bdc06a7);
    8:return('he49b69c1);
    9:return('h0fc19dc6);
    10:return('h2de92c6f);
    11:return('h5cb0a9dc);
    12:return('h983e5152);
    13:return('hb00327c8);
    14:return('hc6e00bf3);
    15:return('h06ca6351);
    16:return('h27b70a85);
    17:return('h4d2c6dfc);
    18:return('h650a7354);
    19:return('h81c2c92e);
    20:return('ha2bfe8a1);
    21:return('hc24b8b70);
    22:return('hd192e819);
    23:return('hf40e3585);
    24:return('h19a4c116);
    25:return('h2748774c);
    26:return('h391c0cb3);
    27:return('h5b9cca4f);
    28:return('h748f82ee);
    29:return('h84c87814);
    30:return('h90befffa);
    31:return('hbef9a3f7);
  endcase
endfunction:k_o

function Bit#(32) k_e(Bit#(6) o);
  case(o)
    0:return('h71374491);
    1:return('he9b5dba5);
    2:return('h59f111f1);
    3:return('hab1c5ed5);
    4:return('h12835b01);
    5:return('h550c7dc3);
    6:return('h80deb1fe);
    7:return('hc19bf174);
    8:return('hefbe4786);
    9:return('h240ca1cc);
    10:return('h4a7484aa);
    11:return('h76f988da);
    12:return('ha831c66d);
    13:return('hbf597fc7);
    14:return('hd5a79147);
    15:return('h14292967);
    16:return('h2e1b2138);
    17:return('h53380d13);
    18:return('h766a0abb);
    19:return('h92722c85);
    20:return('ha81a664b);
    21:return('hc76c51a3);
    22:return('hd6990624);
    23:return('h106aa070);
    24:return('h1e376c08);
    25:return('h34b0bcb5);
    26:return('h4ed8aa4a);
    27:return('h682e6ff3);
    28:return('h78a5636f);
    29:return('h8cc70208);
    30:return('ha4506ceb);
    31:return('hc67178f2);
  endcase
endfunction:k_e

function Bit#(32) rotr (Bit#(32) x,Integer n);
  Bit#(64) y={x,'d0} >> n;
  return y[63:32]|y[31:0];
endfunction:rotr

function Bit#(32) s0 (Bit#(32) x);
  return rotr(x,2)^rotr(x,13)^rotr(x,22);
endfunction:s0

function Bit#(32) s1 (Bit#(32) x);
  return rotr(x,6)^rotr(x,11)^rotr(x,25);
endfunction:s1

function Bit#(32) c0 (Bit#(32) x);
  return rotr(x,7)^rotr(x,18)^(x>>3);
endfunction:c0

function Bit#(32) c1 (Bit#(32) x);
  return rotr(x,17)^rotr(x,19)^(x>>10);
endfunction:c1

function Bit#(32) ch (Bit#(32) x, Bit#(32) y, Bit#(32) z);
  return (x & y)^(~x & z); 
endfunction:ch

function Bit#(32) maj (Bit#(32) x, Bit#(32) y, Bit#(32) z);
  return (x & y)^(x & z)^(y & z);
endfunction:maj

(* synthesize *)
(*mutually_exclusive = "hash,routput"*)
module mksequential_sha_engine(Ifc_sha_engine);
  
  Integer a=0;
  Integer b=1;
  Integer c=2;
  Integer d=3;
  Integer e=4;
  Integer f=5;
  Integer g=6;
  Integer h=7;

  Reg #(Bit#(6))  i <- mkReg(0);
  Reg #(State) state<- mkReg(IDLE);
  Reg #(Bit#(32)) pre_comp[2];
  Reg #(Bit#(32)) w[16];
  Reg #(Bit#(32)) i_h[8];
  Reg #(Bit#(32)) initial_val[8];

  FIFOF #(Bit#(256)) result_fifo <- mkSizedFIFOF(2);

  for(Integer j=0;j<16;j=j+1)
    w[j]<-mkRegU;
  for(Integer j=0;j<8;j=j+1)begin
    i_h[j]<-mkRegU;
    initial_val[j]<-mkRegU;
  end

  pre_comp[0]<-mkRegU;
  pre_comp[1]<-mkRegU;

  function Action updates();action
    Bit#(32) t_11 = i_h[h]+s1(i_h[e])+ch(i_h[e],i_h[f],i_h[g])+pre_comp[0];
    Bit#(32) t_21 = s0(i_h[a])+maj(i_h[a],i_h[b],i_h[c]);
    Bit#(32) tmp1=i_h[d]+t_11;
    Bit#(32) tmp2=t_11+t_21;
    Bit#(32) t_12 = i_h[g]+s1(tmp1)+ch(tmp1,i_h[e],i_h[f])+pre_comp[1];
    Bit#(32) t_22 = s0(tmp2)+maj(tmp2,i_h[a],i_h[b]);
    i_h[h] <= i_h[f];
    i_h[g] <= i_h[e];
    i_h[f] <= tmp1;
    i_h[e] <= i_h[c]+t_12;
    i_h[d] <= i_h[b];
    i_h[c] <= i_h[a];
    i_h[b] <= tmp2;
    i_h[a] <= t_12+t_22;
  endaction
  endfunction:updates

  rule hash(state == HASH);
    `ifdef verbose $display("SE:%8h State:Hash Action:Hash cycle-%8h",cur_cycle,i); `endif
    updates();
    if(i<31)begin
    pre_comp[0]<=w[2]+k_o(i+1);
    pre_comp[1]<=w[3]+k_e(i+1);
    end
    Bit#(32) tmp_w14,tmp_w15;
    tmp_w14=w[0]+w[9]+c0(w[1])+c1(w[14]);
    tmp_w15=w[1]+w[10]+c0(w[2])+c1(w[15]);
    for(Integer j=0;j<14;j=j+1)
      w[j]<=w[j+2];
    w[14]<=tmp_w14;
    w[15]<=tmp_w15;
    if(i==31)
      state<=OUTPUT;
    else
      i <= i+1;
  endrule

  rule routput(state==OUTPUT);
    Bit#(256) res={i_h[a]+initial_val[a],i_h[b]+initial_val[b],i_h[c]+initial_val[c],
                    i_h[d]+initial_val[d],i_h[e]+initial_val[e],i_h[f]+initial_val[f],
                    i_h[g]+initial_val[g],initial_val[h]+i_h[h]};
    result_fifo.enq(res);
    state<=IDLE;
    `ifdef verbose $display("SE:%8h State:Output Action:Output Digest:%64h", cur_cycle,res); `endif
  endrule

  method Bool ready;
    return(state==IDLE);
  endmethod

  method Action reset;
    i <= 0;
    state <= IDLE;
    result_fifo.clear;
  endmethod

  method Action input_engine(Bit#(256) pre_hash,Bit#(512) input_val) if(state==IDLE);
    for(Integer j=0;j<16;j=j+1)
      w[j]<=input_val[512-(j*32)-1:480-(j*32)];
    pre_comp[0]<=input_val[511:480]+k_o(0);
    pre_comp[1]<=input_val[479:448]+k_e(0);
    i_h[a]<=pre_hash[255:224];
    i_h[b]<=pre_hash[223:192];
    i_h[c]<=pre_hash[191:160];
    i_h[d]<=pre_hash[159:128];
    i_h[e]<=pre_hash[127:96];
    i_h[f]<=pre_hash[95:64];
    i_h[g]<=pre_hash[63:32];
    i_h[h]<=pre_hash[31:0];
    initial_val[a]<=pre_hash[255:224];
    initial_val[b]<=pre_hash[223:192];
    initial_val[c]<=pre_hash[191:160];
    initial_val[d]<=pre_hash[159:128];
    initial_val[e]<=pre_hash[127:96];
    initial_val[f]<=pre_hash[95:64];
    initial_val[g]<=pre_hash[63:32];
    initial_val[h]<=pre_hash[31:0];
    i<=0;
    state<=HASH;
    `ifdef verbose $display("SE:%8h State:Idle Action:Input Ph:%64h M:%128h",cur_cycle,pre_hash,input_val); `endif
  endmethod
  interface Get output_engine;
    method ActionValue #(Bit#(256)) get;
    let x=result_fifo.first;
    result_fifo.deq;
    return x;
    endmethod
  endinterface

endmodule:mksequential_sha_engine

endpackage:sequential_sha_engine_unrolled
