package sha_accelerator_v1;

`include "defined_parameters.bsv"

/* ======== Package imports ======= */
import utils         ::*;
import FIFO          ::*;
import ConfigReg     ::*;
import Connectable   ::*;
import GetPut        ::*;
import DefaultValue  ::*;
/*================================== */

/*========= Project imports ======== */
import AXI4_Types		        :: *;
import AXI4_Fabric	        :: *;
import pipeline_sha_engine  :: *;
import Semi_FIFOF           :: *;
/*================================== */

/*
0-Read Addr
1-Write Addr
2-No of read bursts=no of entries/(2*(256/passlengthinbits))
3-Pass Length in bytes
*/
typedef Bit#(`PADDR) Req_Addr;
typedef Bit#(`Reg_width) Req_Data;
typedef Bit#(`USERSPACE) Req_Info;

function Reg#(Bit#(n)) regAToRegBitN( Reg#(a_type) rin )
	provisos ( Bits#( a_type, asize),
			   Add#(asize,xxx,n) ) ;

	return
	interface Reg
		method Bit#(n) _read ();
			a_type tmp =  rin._read()  ;
			return zeroExtend (pack( tmp )) ;
		endmethod
		method Action _write( Bit#(n) din );
			rin._write( unpack( truncate(din) )) ;
		endmethod
	endinterface ;
endfunction

function Bit#(3) readSize ( Bit#(64) length);
  case (length)
    'd8: return 'd4;
    'd16:return 'd5;
    'd32:return 'd6;
  endcase
endfunction:readSize

interface Ifc_accelerator;
  interface AXI4_Master_IFC#(`PSHAADDR, 512, `USERSPACE) data_channel;
  interface AXI4_Slave_IFC#(`PADDR, `Reg_width, `USERSPACE) config_channel;
endinterface

module mkAccelerator(Ifc_accelerator);
  AXI4_Slave_Xactor_IFC #(`PADDR,`Reg_width,`USERSPACE) s_xactor <- mkAXI4_Slave_Xactor;
  AXI4_Master_Xactor_IFC #(`PSHAADDR,512,`USERSPACE) m_xactor <- mkAXI4_Master_Xactor;

  Reg#(Bit#(`Reg_width))   config_regs[4];
  Reg#(Bit#(1))            status <- mkReg(0);
  Reg#(Bit#(`Reg_width))   read_transact_counter <- mkReg(0);
  Reg#(Bit#(`Reg_width))   write_transact_counter <- mkReg(0);
  Reg#(Bit#(8))            index <- mkReg(0);
  Reg#(Bit#(1))            prev_cond<- mkReg(1);
  Reg#(Bit#(0))            nullReg <- mkReg( ? ) ;
  Reg#(Bit#(64))           readAddress<-mkReg(0);
  
  Ifc_sha_engine engine[`Num_of_engines];

  for(Integer i=0;i<4;i=i+1)
    config_regs[i]<-mkRegU;
  for(Integer i=0;i<`Num_of_engines;i=i+1)
    engine[i]<-mkpipeline_sha_engine;

  function Tuple2#(Reg#(Req_Data), Bool) selectReg(Req_Addr addr);
    Bit#(8) taddr = truncate(addr);
    case(taddr)
      ((`Reg_width/4)*0) : return tuple2(regAToRegBitN(status),True);
      ((`Reg_width/4)*1) : return tuple2(regAToRegBitN(config_regs[0]),True);
      ((`Reg_width/4)*2) : return tuple2(regAToRegBitN(config_regs[1]),True);
      ((`Reg_width/4)*3) : return tuple2(regAToRegBitN(config_regs[2]),True);
      ((`Reg_width/4)*4) : return tuple2(regAToRegBitN(config_regs[3]),True);
      default      : return tuple2(regAToRegBitN(nullReg),False);
    endcase
  endfunction:selectReg

  rule writeConfig(status=='h0);
    let write_addr <- pop_o(s_xactor.o_wr_addr);
    let write_data <- pop_o(s_xactor.o_wr_data);
    Req_Data lv_data= 0;
    Req_Addr lv_addr= 0;
    AXI4_Resp lv_bresp= AXI4_OKAY;
    Bool lv_send_response= True;

    if(write_data.wstrb=='hF0) begin
      lv_data= zeroExtend(write_data.wdata[63:32]);
      lv_addr= write_addr.awaddr;
    end
    else if(write_data.wstrb=='h0F) begin
      lv_data= zeroExtend(write_data.wdata[31:0]);
      lv_addr= write_addr.awaddr;
    end
    else if(write_data.wstrb=='hFF) begin
      lv_data= write_data.wdata;
      lv_addr= write_addr.awaddr;
    end
    else begin
      lv_bresp= AXI4_SLVERR;
      $display($time,"\tDMA: KAT GAYA");
    end
    
    let lv1= selectReg(lv_addr);
    let thisReg = tpl_1(lv1);
    if(!tpl_2(lv1)) begin	//if no register mapping exists for the given address
      lv_bresp= AXI4_SLVERR;
      $display($time,"\tDMA: Wapas KAT GAYA");
    end
    else begin
      lv_bresp = AXI4_OKAY;
      thisReg <= lv_data;
      let resp = AXI4_Wr_Resp { bresp: lv_bresp, buser: 0, bid: write_addr.awid };
      s_xactor.i_wr_resp.enq(resp);
    end
    if(lv_addr=='h0)begin
      readAddress<=config_regs[0];
      engine[1].reset;
      engine[0].reset;
    end
  endrule

  rule createReadTransactions(status=='h1);
    if(read_transact_counter<config_regs[2])begin
    let read_request = AXI4_Rd_Addr {araddr: readAddress, 
    arid: `Sha_accelerator_id, arlen: 'hFF,
    arsize: readSize(config_regs[3]), arburst: 'd1, //arburst: 00-FIXED 01-INCR 10-WRAP
    aruser: 0 };
    m_xactor.i_rd_addr.enq(read_request);
    read_transact_counter<=read_transact_counter+1;
    readAddress<=readAddress+(config_regs[3]<<9);
    end
  endrule

  rule serviceReadResponses((status=='h1)&&
        (m_xactor.o_rd_data.first.rid==`Sha_accelerator_id));
    if(engine[0].ready&&engine[1].ready)begin
      Bit#(512) input1,input2;
      if(config_regs[3]==8)begin
      input2={m_xactor.o_rd_data.first.rdata[127:64]
                          ,8'h80,'d0,config_regs[3]<<'d3};
      input1={m_xactor.o_rd_data.first.rdata[63:0]
                          ,8'h80,'d0,(config_regs[3]<<3)};
      end
      else if(config_regs[3]==16)begin
      input2={m_xactor.o_rd_data.first.rdata[255:128]
                          ,8'h80,'d0,(config_regs[3]<<3)};
      input1={m_xactor.o_rd_data.first.rdata[127:0]
                          ,8'h80,'d0,(config_regs[3]<<3)};
      end
      else begin
      input2={m_xactor.o_rd_data.first.rdata[511:256]
                          ,8'h80,'d0,(config_regs[3]<<3)};
      input1={m_xactor.o_rd_data.first.rdata[255:0]
                          ,8'h80,'d0,(config_regs[3]<<3)};
      end
      Bit#(256) ph='h6a09e667bb67ae853c6ef372a54ff53a510e527f9b05688c1f83d9ab5be0cd19;
      engine[0].input_engine(ph,input1);
      engine[1].input_engine(ph,input2);
      m_xactor.o_rd_data.deq;
    end
  endrule

  rule createWriteTransactions(status=='h1);
    if((write_transact_counter>>'d8)<=config_regs[2])begin 
      let output1<-engine[1].output_engine.get;
      let output0<-engine[0].output_engine.get;
      if(prev_cond=='b1)begin
      let write_data = AXI4_Wr_Data{wdata:{output1,output0},
                                    wstrb:'hffffffffffffffff,wid:`Sha_accelerator_id,wlast:False};
      let write_request = AXI4_Wr_Addr { awaddr:config_regs[0]+write_transact_counter<<7,
      awuser:0, awlen:'hFF,awsize:'d6,
      awburst: 'd1,
      awid:`Sha_accelerator_id};
      m_xactor.i_wr_addr.enq(write_request);
      m_xactor.i_wr_data.enq(write_data);
      write_transact_counter<=write_transact_counter+1;
      prev_cond<=0;
      end
      else if((write_transact_counter[0]&write_transact_counter[1]&write_transact_counter[2]&
          write_transact_counter[3]&write_transact_counter[4]&write_transact_counter[5]&
          write_transact_counter[6]&write_transact_counter[7])==1)begin
        let write_data = AXI4_Wr_Data{wdata:{output1,output0},
                                      wstrb:'hffffffffffffffff,wid:`Sha_accelerator_id,wlast:True};  
        m_xactor.i_wr_data.enq(write_data);
        write_transact_counter<=write_transact_counter+1;
        prev_cond<=1;
      end
      else begin
        let write_data = AXI4_Wr_Data{wdata:{output1,output0},
                                      wstrb:'hffffffffffffffff,wid:`Sha_accelerator_id,wlast:False};  
        m_xactor.i_wr_data.enq(write_data);
        write_transact_counter<=write_transact_counter+1;
      end
    end
  endrule
  
  rule deqWriteResp(m_xactor.o_wr_resp.first.bid==`Sha_accelerator_id);
    m_xactor.o_wr_resp.deq;
  endrule

  rule retIdle((status=='h1)&&((write_transact_counter>>'d8)==config_regs[2]));
    write_transact_counter<=0;
    read_transact_counter<=0;
    status<=0;
    index<=0;
    prev_cond<=1;
  endrule 

  interface config_channel=s_xactor.axi_side;
  interface data_channel=m_xactor.axi_side;
endmodule:mkAccelerator
endpackage:sha_accelerator_v1